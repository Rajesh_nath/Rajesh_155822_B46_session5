<?php
//indexed array    php do auto indexing;
$myArray=array(13,true,75.35,"hell0",5);
echo "<pre>";
print_r($myArray);
echo "<pre>";

echo "<pre>";
var_dump($myArray);

echo "<pre>";


//associated array  we can defined indexing
 $personAge = array("10"=>45,"Rahim"=>35, "Karim"=>45, "Jorina"=>18,6);
echo "<pre>";
print_r($personAge);
echo "<pre>";

echo "<pre>";
var_dump($personAge);

echo "<pre>";

echo $personAge['Jorina']."<br>";
echo $personAge[1];